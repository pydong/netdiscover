# -*- coding: utf-8 -*-
import netsnmp
from multiprocessing import Pool


def snmp(ip):
    ss = netsnmp.Session(Version=2, DestHost=ip, Community="public")
    var_list = netsnmp.VarList(netsnmp.Varbind('sysDescr', 0),
                               netsnmp.Varbind('sysName', 0),
                               netsnmp.Varbind('sysLocation', 0)
                               )
    res = ss.get(var_list)
    return res


def snmp_check(ip):
    ss = netsnmp.Session(Version=2, DestHost=ip, Community="public")
    var_list = netsnmp.VarList(netsnmp.Varbind('ipForwarding', 0))
    res = ss.get(var_list)
    return res



def snmpwalk(oid, ip):
    oid = netsnmp.Varbind(oid)
    result = netsnmp.snmpwalk(oid,
                              Version=2,
                              DestHost=ip,
                              Community="public")
    return result


def getmbiinfo(ips):
    result = []
    p = Pool(10)
    for ip in ips:
        for item in ip:
            print(item)
            result.append(p.apply_async(snmp, args=(str(item),)))
    p.close()
    p.join()
    result_new = []
    for res in result:
        result_new.append(res.get())
    return result_new


if __name__ == '__main__':
    from netinfo import *
    snmpwalk('.1.3.6.1.2.1.3.1.1.3', '192.168.3.1')